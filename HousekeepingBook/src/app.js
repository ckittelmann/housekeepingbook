System.register([], function(exports_1) {
    var App;
    return {
        setters:[],
        execute: function() {
            class App {
                configureRouter(config, router) {
                    config.title = 'Housekeeping book';
                    config.map([
                        { route: ['', 'overview'], name: 'overview', moduleId: './Overview/overview', nav: true, title: 'Overview' },
                        { route: ['booking'], name: 'booking', moduleId: './Booking/bookings', nav: true, title: 'Bookings' },
                        { route: ['category'], name: 'category', moduleId: './Category/categories', nav: true, title: 'Categories' },
                        { route: ['account'], name: 'account', moduleId: './Account/accounts', nav: true, title: 'Accounts' }
                    ]);
                    this.router = router;
                }
            }
            App = App;
        }
    }
});
//# sourceMappingURL=app.js.map