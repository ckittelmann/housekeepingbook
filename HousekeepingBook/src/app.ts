﻿import {Router} from "aurelia-router";
import {RouterConfiguration} from "aurelia-router";

export class App {
    router: Router;
    configureRouter(config: RouterConfiguration, router: Router) {
        config.title = 'Housekeeping book';
        config.map([
            { route: ['', 'overview'], name: 'overview', moduleId: './Overview/overview', nav: true, title: 'Overview' },
            { route: ['booking'], name: 'booking', moduleId: './Booking/bookings', nav: true, title: 'Bookings' },
            { route: ['category'], name: 'category', moduleId: './Category/categories', nav: true, title: 'Categories' },
            { route: ['account'], name: 'account', moduleId: './Account/accounts', nav: true, title: 'Accounts' }
        ]);
        this.router = router;
    }
}