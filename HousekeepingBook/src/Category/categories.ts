﻿import {inject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";

@inject(HttpClient)
export class Categories {
    categories:any = [];
    id = "";
    title = "";

    constructor(private http: HttpClient) {
        this.http = http;
    }

    activate() {
        return this.http.fetch("api/categories")
            .then(response => response.json())
            .then(data => { this.categories = data;}); 
    }
}