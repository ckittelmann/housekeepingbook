System.register(["aurelia-framework", "aurelia-fetch-client"], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var aurelia_framework_1, aurelia_fetch_client_1;
    var Categories;
    return {
        setters:[
            function (aurelia_framework_1_1) {
                aurelia_framework_1 = aurelia_framework_1_1;
            },
            function (aurelia_fetch_client_1_1) {
                aurelia_fetch_client_1 = aurelia_fetch_client_1_1;
            }],
        execute: function() {
            let Categories = class {
                constructor(http) {
                    this.http = http;
                    this.categories = [];
                    this.id = "";
                    this.title = "";
                    this.http = http;
                }
                activate() {
                    return this.http.fetch("api/categories")
                        .then(response => response.json())
                        .then(data => { this.categories = data; });
                }
            };
            Categories = __decorate([
                aurelia_framework_1.inject(aurelia_fetch_client_1.HttpClient)
            ], Categories);
            Categories = Categories;
        }
    }
});
//# sourceMappingURL=categories.js.map