﻿namespace HousekeepingBook.Models
{
	using System.ComponentModel.DataAnnotations;

	public class Category : AbstractModel
	{
		[Required]
		public string Title { get; set; }

		public bool IsExpensesCategory { get; set; }
	}
}